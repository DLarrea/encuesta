import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EncuestaComponent } from './encuesta/encuesta.component';


const routes: Routes = [
  {
    path: "",
    redirectTo: "encuestas",
    pathMatch: "full"
  },
  {
    path: "encuestas",
    component: EncuestaComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
