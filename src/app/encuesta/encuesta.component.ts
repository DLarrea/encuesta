import { Component, OnInit } from '@angular/core';
import Stepper from 'bs-stepper'

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.scss']
})
export class EncuestaComponent implements OnInit {

  stepper = null;
  constructor() { }

  ngOnInit(): void {
    this.stepper = new Stepper(document.querySelector('.bs-stepper'));
  }


  next(){
    this.stepper.next();
  }

  previous(){
    this.stepper.previous();
  }
}
